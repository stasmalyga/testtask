﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ConsoleApp
{
    class AppDatabase
    {
        private string databaseConnectionString;

        public AppDatabase(string databaseConnectionString)
        {
            this.databaseConnectionString = databaseConnectionString;
        }

        // Selects books with popularity rating equal to or above the provided parameter, and a limit.
        public List<Book> SelectBooks(int popularityRating, int limit)
        {
            var bookList = new List<Book>();
            using (var connection = new SqlConnection(databaseConnectionString))
            {
                connection.Open();

                string sql = "SELECT BookName, AuthorFirstName, AuthorLastName, PublishDate, PageQuantity, Rating, Price " +
                    "FROM Book WHERE Rating >= @Rating ORDER BY Rating DESC, Price / PageQuantity ASC " +
                    "OFFSET 0 ROWS FETCH NEXT @Limit ROWS ONLY";
                var select = new SqlCommand(sql, connection);
                select.Parameters.AddWithValue("@Rating", popularityRating);
                select.Parameters.AddWithValue("@Limit", limit);
                using (SqlDataReader reader = select.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        bookList.Add(CreateBook(reader));
                    }
                }

                connection.Close();
            }
            return bookList;
        }

        // Inserts a list of books and updates existing books with the same author name and title.
        public void InsertBooks(List<Book> bookList)
        {
            using (var connection = new SqlConnection(databaseConnectionString))
            {
                connection.Open();

                // check for books with the same name and author
                string sqlSelect = "SELECT Id FROM dbo.Book WHERE " +
                    "BookName = @BookName " +
                    "AND AuthorFirstName = @AuthorFirstName " +
                    "AND AuthorLastName = @AuthorLastName";
                var select = new SqlCommand(sqlSelect, connection);
                select.Parameters.Add("@BookName", SqlDbType.NVarChar, 500);
                select.Parameters.Add("@AuthorFirstName", SqlDbType.NVarChar, 50);
                select.Parameters.Add("@AuthorLastName", SqlDbType.NVarChar, 50);

                // insert if the book is new
                string sqlInsert = "INSERT INTO dbo.Book " +
                    "(BookName, AuthorFirstName, AuthorLastName, PublishDate, PageQuantity, Rating, Price) " +
                    "VALUES (@BookName, @AuthorFirstName, @AuthorLastName, @PublishDate, @PageQuantity, @Rating, @Price)";
                var insert = new SqlCommand(sqlInsert, connection);
                insert.Parameters.Add("@BookName", SqlDbType.NVarChar, 500);
                insert.Parameters.Add("@AuthorFirstName", SqlDbType.NVarChar, 50);
                insert.Parameters.Add("@AuthorLastName", SqlDbType.NVarChar, 50);
                insert.Parameters.Add("@PublishDate", SqlDbType.Date);
                insert.Parameters.Add("@PageQuantity", SqlDbType.Int);
                insert.Parameters.Add("@Rating", SqlDbType.Int);
                insert.Parameters.Add("@Price", SqlDbType.Decimal);

                // update if the book exists in the table
                string sqlUpdate = "UPDATE dbo.Book SET PublishDate = @PublishDate, " +
                    "PageQuantity = @PageQuantity, Rating = @Rating, Price = @Price, " +
                    "Updated = CURRENT_TIMESTAMP WHERE Id = @id";
                var update = new SqlCommand(sqlUpdate, connection);
                update.Parameters.Add("@Id", SqlDbType.Int);
                update.Parameters.Add("@PublishDate", SqlDbType.Date);
                update.Parameters.Add("@PageQuantity", SqlDbType.Int);
                update.Parameters.Add("@Rating", SqlDbType.Int);
                update.Parameters.Add("@Price", SqlDbType.Decimal);

                foreach (var book in bookList)
                {
                    select.Parameters["@BookName"].Value = book.BookName;
                    select.Parameters["@AuthorFirstName"].Value = book.AuthorFirstName;
                    select.Parameters["@AuthorLastName"].Value = book.AuthorLastName;
                    bool bookExists = false;
                    int id = 0;
                    using (SqlDataReader reader = select.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            bookExists = true;
                            id = (int)reader["Id"];
                        }
                    }

                    if (bookExists)
                    {
                        update.Parameters["@Id"].Value = id;
                        update.Parameters["@PublishDate"].Value = book.PublishDate.ToString("yyyy-MM-dd");
                        update.Parameters["@PageQuantity"].Value = book.PageQuantity;
                        update.Parameters["@Rating"].Value = book.Rating;
                        update.Parameters["@Price"].Value = book.Price;
                        update.ExecuteNonQuery();
                    }
                    else
                    {
                        insert.Parameters["@BookName"].Value = book.BookName;
                        insert.Parameters["@AuthorFirstName"].Value = book.AuthorFirstName;
                        insert.Parameters["@AuthorLastName"].Value = book.AuthorLastName;
                        insert.Parameters["@PublishDate"].Value = book.PublishDate.ToString("yyyy-MM-dd");
                        insert.Parameters["@PageQuantity"].Value = book.PageQuantity;
                        insert.Parameters["@Rating"].Value = book.Rating;
                        insert.Parameters["@Price"].Value = book.Price;
                        insert.ExecuteNonQuery();
                    }
                }

                connection.Close();
            }
        }

        // Logs a file access attempt.
        public void InsertFileLog(string fileName, bool isError, string message)
        {
            using (var connection = new SqlConnection(databaseConnectionString))
            {
                connection.Open();
                string sql = "INSERT INTO FileLog (FileName, IsError, Message) " +
                    "VALUES (@FileName, @IsError, @Message)";
                var insert = new SqlCommand(sql, connection);
                insert.Parameters.Add("@FileName", SqlDbType.NVarChar, 500);
                insert.Parameters.Add("@IsError", SqlDbType.Bit);
                insert.Parameters.Add("@Message", SqlDbType.NVarChar, 500);
                insert.Parameters["@FileName"].Value = fileName;
                insert.Parameters["@IsError"].Value = isError;
                insert.Parameters["@Message"].Value = message;
                insert.ExecuteNonQuery();
                connection.Close();
            }
        }

        protected Book CreateBook(SqlDataReader reader)
        {
            return new Book
            {
                BookName = (string)reader["BookName"],
                AuthorFirstName = (string)reader["AuthorFirstName"],
                AuthorLastName = (string)reader["AuthorLastName"],
                PublishDate = (DateTime)reader["PublishDate"],
                PageQuantity = (int)reader["PageQuantity"],
                Rating = (int)reader["Rating"],
                Price = (decimal)reader["Price"]
            };
        }
    }
}
