﻿using System;
using System.Configuration;
using System.Collections.Specialized;

namespace ConsoleApp
{
    class AppConfiguration
    {
        public NameValueCollection ReadSettings { get; private set; }
        public NameValueCollection WriteSettings { get; private set; }
        public string DatabaseConnectionString { get; private set; }

        public AppConfiguration()
        {
            ReadSettings = (NameValueCollection)ConfigurationManager.GetSection("readSettings");
            if (ReadSettings == null)
            {
                throw new Exception("Error reading configuration file. Section 'readSettings' could not be found.");
            }

            WriteSettings = (NameValueCollection)ConfigurationManager.GetSection("writeSettings");
            if (WriteSettings == null)
            {
                throw new Exception("Error reading configuration file. Section 'writeSettings' could not be found.");
            }

            try
            {
                DatabaseConnectionString = ConfigurationManager.ConnectionStrings["databaseConnectionString"].ConnectionString;
            }
            catch
            {
                throw new Exception("Error reading configuration file. Parameter 'databaseConnectionString' could not be found.");
            }
        }
    }
}
