﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Xml;

namespace ConsoleApp
{
    class FileWriter
    {
        public string FilePath { get; private set; }
        public int PopularityRating { get; private set; }

        private string orderDate = "";

        public FileWriter(NameValueCollection config)
        {
            orderDate = DateTime.Now.ToString("yyyyMMdd");
            FilePath = config["filePath"].Replace("%date%", orderDate);
            if (FilePath == "")
            {
                throw new Exception("Error: File path cannot be empty.");
            }

            if (Int32.TryParse(config["popularityRating"], out int popularityRating))
            {
                PopularityRating = popularityRating;
            }
            else
            {
                throw new Exception("Error: 'popularityRating' must be an integer.");
            }
        }

        // Creates or owerwrites an XML file located at FilePath.
        public void Write(List<Book> bookList)
        {
            var document = new XmlDocument();

            XmlNode declaration = document.CreateXmlDeclaration("1.0", "utf-8", "yes");
            document.AppendChild(declaration);

            XmlNode orders = document.CreateElement("Orders");
            document.AppendChild(orders);

            XmlNode orderNumber = document.CreateElement("OrderNumber");
            orderNumber.InnerText = "Order_" + orderDate;
            orders.AppendChild(orderNumber);

            XmlNode docDate = document.CreateElement("OrderNumber");
            docDate.InnerText = orderDate;
            orders.AppendChild(docDate);

            XmlNode orderElements = document.CreateElement("OrderElements");
            orders.AppendChild(orderElements);

            foreach (var book in bookList)
            {
                XmlNode orderElement = document.CreateElement("OrderElement");
                orderElements.AppendChild(orderElement);

                XmlNode bookName = document.CreateElement("Book_Name");
                bookName.InnerText = book.BookName;
                orderElement.AppendChild(bookName);

                XmlNode authorFirstName = document.CreateElement("Writer_FisrtName");
                authorFirstName.InnerText = book.AuthorFirstName;
                orderElement.AppendChild(authorFirstName);

                XmlNode authorLastName = document.CreateElement("Writer_LastName");
                authorLastName.InnerText = book.AuthorLastName;
                orderElement.AppendChild(authorLastName);

                XmlNode bookDetails = document.CreateElement("Book_details");
                orderElement.AppendChild(bookDetails);

                XmlNode publishDate = document.CreateElement("Publishdate");
                publishDate.InnerText = book.PublishDate.ToString("yyyy-MM-dd");
                bookDetails.AppendChild(publishDate);

                XmlNode pricePerPage = document.CreateElement("PagePrice");
                pricePerPage.InnerText = book.PricePerPage.ToString();
                bookDetails.AppendChild(pricePerPage);
            }

            document.Save(FilePath);
        }
    }
}
