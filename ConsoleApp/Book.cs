﻿using System;

namespace ConsoleApp
{
    // Represents a single book
    class Book
    {
        public string BookName { get; set; }
        public string AuthorFirstName { get; set; }
        public string AuthorLastName { get; set; }
        public DateTime PublishDate { get; set; }
        public int PageQuantity { get; set; } = 0;
        public int Rating { get; set; } = 0;
        public decimal Price { get; set; } = 0;
        public decimal PricePerPage
        {
            get
            {
                if (PageQuantity > 0)
                {
                    return Decimal.Round(Price / PageQuantity, 4);
                }
                else
                {
                    return 0;
                }
            }
        }
    }
}
