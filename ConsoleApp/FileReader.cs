﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace ConsoleApp
{
    class FileReader
    {
        public string FilePath { get; private set; }

        // CSV file columns
        private enum CsvSchema
        {
            BookName,
            AuthorFirstName,
            AuthorLastName,
            PublishDate,
            PageQuantity,
            Rating,
            Price
        }

        public FileReader(NameValueCollection config)
        {
            FilePath = config["filePath"];
            if (FilePath == "")
            {
                throw new Exception("Error: File path cannot be empty.");
            }
        }

        // Reads the CSV file specified in FilePath.
        public List<Book> Read()
        {
            var bookList = new List<Book>();

            using (var reader = new StreamReader(FilePath))
            {
                reader.ReadLine(); // ignores the first line
                int lineNumber = 2; // line index, including the first line
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();

                    try
                    {
                        bookList.Add(CreateBook(line));
                    }
                    catch
                    {
                        throw new Exception("Line #" + lineNumber + " cannot be parsed. Contents: " + line);
                    }
                    lineNumber++;
                }
            }
            return bookList;
        }

        protected Book CreateBook(string line)
        {
            int columnCount = Enum.GetNames(typeof(CsvSchema)).Length;

            string[] cells = line.Split(';');

            // if splitting by semicolon fails splits by comma
            if (cells.Length != columnCount)
            {
                cells = line.Split(',');
                // if cannot get a correct number of columns throws an exception
                if (cells.Length != columnCount)
                {
                    throw new Exception(); // rethrows in Read() with a proper message
                }
            }

            return new Book
            {
                BookName = cells[(int)CsvSchema.BookName],
                AuthorFirstName = cells[(int)CsvSchema.AuthorFirstName],
                AuthorLastName = cells[(int)CsvSchema.AuthorLastName],
                PublishDate = Convert.ToDateTime(cells[(int)CsvSchema.PublishDate]),
                PageQuantity = Int32.Parse(cells[(int)CsvSchema.PageQuantity]),
                Rating = Int32.Parse(cells[(int)CsvSchema.Rating]),
                Price = Decimal.Parse(cells[(int)CsvSchema.Price])
            };
        }
    }
}
