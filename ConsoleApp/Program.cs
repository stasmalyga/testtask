﻿using System;
using System.Collections.Generic;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var config = new AppConfiguration();
                var database = new AppDatabase(config.DatabaseConnectionString);

                string selectedOption;
                if (args.Length > 0)
                {
                    // using the first provided argument as the selected option
                    selectedOption = args[0];
                }
                else
                {
                    Console.WriteLine("Select an option:");
                    Console.WriteLine();
                    Console.WriteLine("1. Load File");
                    Console.WriteLine("2. Create Order");
                    Console.WriteLine();

                    selectedOption = Console.ReadLine().Trim();
                }

                if (selectedOption == "1")
                {
                    LoadFile(config, database);
                }
                else if (selectedOption == "2")
                {
                    CreateOrder(config, database);
                }
                else
                {
                    Console.WriteLine("Please select an available option.");
                }

                Console.WriteLine("Press any key to exit.");
                Console.ReadKey();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Press any key to exit.");
                Console.ReadKey();
            }
        }

        // option 1. Load File
        private static void LoadFile(AppConfiguration config, AppDatabase database)
        {
            var fileReader = new FileReader(config.ReadSettings);
            try
            {
                List<Book> bookList = fileReader.Read();
                database.InsertBooks(bookList);
            }
            catch (Exception exception)
            {
                database.InsertFileLog(fileReader.FilePath, true, exception.Message);
                throw;
            }
            database.InsertFileLog(fileReader.FilePath, false, "File imported successfully.");

            Console.WriteLine("File " + fileReader.FilePath + " was successfully loaded.");
        }

        // option 2. Create Order
        private static void CreateOrder(AppConfiguration config, AppDatabase database)
        {
            int bookLimit = 100;
            var fileWriter = new FileWriter(config.WriteSettings);
            List<Book> bookList = database.SelectBooks(fileWriter.PopularityRating, bookLimit);
            fileWriter.Write(bookList);
            Console.WriteLine("File " + fileWriter.FilePath + " with popularity rating threshold of " + fileWriter.PopularityRating + " was successfully created.");
        }
    }
}
