Notes:

-----

## Configuration file

I used App.config, an application configuration option provided by Visual Studio by default. It's an XML file with three settings sections:

1. connectionStrings, contains the database connection string. This section is supported by System.Config by default. databaseConnectionString is set to connect to the database project in the same solution;

2. readSettings, a custom section containing a path to an existing CSV file which will be loaded if the 'Load File' option is selected;

3. writeSettings, a custom section containing a path to an XML file which will be created or overwritten if the 'Create Order' option is selected; and a popularityRating parameter used for XML export. 'filePath' may contain a %date% substring that will be replaced by the current date;

App.config is loaded when the application starts. Note: VS will copy the file into bin/Debug, the current configuration strings assume the files directory is in the solution root, ../../../Files/ relatively to the config location.

## Error handling

The application catches exceptions and outputs messages as plain text, suspending the execution.

The application fails on startup when readSettings, writeSettings configuration sections or databaseConnectionString option could not be found.

The application fails when a correct option is selected but a corresponding config section doesn't contain valid parameters (empty file path, non-integer rating).

The file reader assumes the CSV file is semicolon-delimited, if a line cannot be split in the required number of columns it attempts to split by comma; if neither provides a correct number of columns it throws an error. Note that currently it won't parse attempts to escape delimiters, e.g. 'Col1;"Col;2";Col3' would be interpreted as four columns. When the CSV reader fails it outputs the number and the contents of the problem string.

The log table saves successful and unsuccessful attempts of CSV file access. It doesn't log XML Order writing attempts - the task description did not ask for it so I wasn't sure if it's needed. If the file was interpreted and copied successfully it will copy a success note, otherwise it will mark the record as an error and insert the exception message.

## Performance and Execution

The application uses raw parametrized SQL queries. I know I could use EF but it wasn't clear which option is preferable.

Book import traverses through the list, checks each book for duplicates by comparing book and author name, and inserts or updates the book. In a previous version I had a database trigger that would do that but it was noticeably slower. I could not find a more straightforward solution for update-on-duplicate yet.

-----

The solution is developed in VS 2017, targeting .Net Framework 4.7.1 for the console app and SQL Server 2016 for the database project.
